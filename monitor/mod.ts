import { connection } from '../redis/mod.ts'
import { sleep, Job } from '../common/mod.ts'
import { getHashObject, loop } from '../redis/util.ts'

export type MonitorDesc = {
  queue: string
  interval?: number
}

const monitor = async ({ queue, interval = 500 }: MonitorDesc) => {
  const redis = await connection()
  const inProgressQueue = `queue:${queue}:in-progress`

  loop(async () => {
    const jobKeys = await redis.lrange(inProgressQueue, 0, -1) as string[]
    const promises = jobKeys.map(key => 
      redis.transaction(async trx => {
        await trx.watch(key)

        const job = await getHashObject<Job>(trx, key, {
          timeout: 'number',
          expire: 'number',
          tries: 'number',
          name: 'string'
        })

        if ( job === null ) {
          console.warn('Job found in-progress that no longer exists')
          return trx.multi(pl => pl.lrem(inProgressQueue, 1, key))
        }

        const { timeout, expire, tries, name } = job

        const jobQueue = `queue:${queue}:${name}`
        await trx.watch(jobQueue)

        const now = Date.now()
        if ( expire > now ) {
          return false
        }

        trx.multi(pl => {
          pl.hset(key, ['expire', now + timeout], ['tries', tries + 1])
          pl.lrem(inProgressQueue, 1, key)
          pl.lpush(jobQueue, key)
        })
      }).then(({ executed }) => (executed !== false) &&
        console.log(`Job moved back into queue: ${key}`)))

    return Promise.all(promises)
      .then(() => sleep(interval))
  })
}

export default monitor
