import { config } from 'https://deno.land/x/dotenv/mod.ts'
config({ export: true })

import producer from './producer/mod.ts'
import consumer from './consumer/mod.ts'
import monitor from './monitor/mod.ts'

const test = async () => {
  const queue = 'job-queue'

  const q1 = await producer({ queue })
  await q1.add({
    name: 'add',
    data: 'DATA',
    timeout: 10000
  })

  monitor({ queue })

  const q2 = await consumer({ queue, name: 'add' })
  for ( ;; ) {
    const job = await q2.wait()
    console.log('got job', job)
  }
}

test()
