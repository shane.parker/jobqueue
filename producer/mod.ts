import { connection } from '../redis/mod.ts'

export type ProducerInit = {
  queue: string
}

export type JobInit = {
  name: string,
  data: string,
  timeout?: number
}

export type JobProducer = {
  add: (job: JobInit) => Promise<string>
}

const producer = async ({ queue }: ProducerInit): Promise<JobProducer> => {
  const redis = await connection()

  return {
    add: ({ name, data, timeout = 10000 }) => {
      const qid = `queue:${queue}:${name}`
      const id = `job:${crypto.randomUUID()}`
      const now = Date.now()

      timeout = (timeout > 0) ? timeout : 10000

      const pl = redis.multi()
      pl.hset(id,
        ['date', now],
        ['timeout', timeout],
        ['expire', now+timeout],
        ['name', name],
        ['tries', 0],
        ['data', data!.toString()])
      pl.lpush(qid, id)
      return pl.exec().then(() => id)
    }
  }
}

export default producer
