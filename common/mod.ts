export const JobSchema = {
  'date': 'number',
  'expire': 'number',
  'timeout': 'number',
  'tries': 'number',
  'name': 'string',
  'data': 'string'
}

export type Job = {
  name: string,
  data: string,
  timeout: number,
  date: number,
  expire: number,
  tries: number
}

export const sleep = (timeout: number) =>
  new Promise(resolve => setTimeout(resolve, timeout))
