import {
  RedisPromise,
  RedisValue,
  KeyValue,
  RedisBasicValue
} from './types.ts'

export type MoveDirection = 'right' | 'left'

type RedisSender<T> = {
  send: (cmd: string, ...args: RedisValue[]) => T
}

type RedisCmds<T> = {
  set: (key: string, val: string) => T
  get: (key: string) => T

  hset: (key: string, ...entries: KeyValue[]) => T
  hget: (key: string, ...keys: string[]) => T

  lpush: (key: string, value: string) => T
  lpop: (key: string, value: string) => T
  lrem: (key: string, count: number, value: string) => T
  lrange: (key: string, start: number, end: number) => T

  blmove: (src: string, dst: string, dir1: MoveDirection, dir2: MoveDirection, timeout?: number) => T
}

export type Redis<T> = RedisSender<T> & RedisCmds<T>

const redisBase: Redis<unknown> = {
  send (_cmd, _args) {
    throw Error('Send not implemented')
  },

  set (key, val: string) { return this.send('SET', key, val) },
  get (key) { return this.send('GET', key) },

  hset (key, ...entries) {
    const params = entries.reduce((acc, [k, v]) => {
      acc.push(k, v)
      return acc
    }, [] as RedisBasicValue[])

    return this.send('HSET', key, ...params)
  },
  hget (key, ...keys) { return this.send('HMGET', key, ...keys) },

  lpush (key, value) { return this.send('LPUSH', key, value) },
  lpop (key, value) {return this.send('LPOP', key, value) },
  lrem (key, count, value) { return this.send('LREM', key, count, value) },
  lrange (key, start, end) { return this.send('LRANGE', key, start, end) },

  blmove (src: string, dst: string, dir1: MoveDirection, dir2: MoveDirection, timeout?: number) {
    return this.send('BLMOVE', src, dst, dir1, dir2, timeout ?? 0)
  }
}

export const makeRedis = <T, R>(overrides: T & RedisSender<R>) =>
  Object.setPrototypeOf(overrides, redisBase) as (Redis<R> & T)
