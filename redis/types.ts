export type HashObject = { [key: string]: RedisBasicValue }
export type RedisBasicValue = number | string | false | undefined | void
export type RedisValue = RedisBasicValue | RedisValue[]
export type RedisPromise = Promise<RedisValue>
export type KeyValue = [k: string, v: RedisBasicValue]
export type DefaultObject = { [key:string]: unknown }

export type TransactionOptions = {
  watch?: string[]
  retries?: number
}
