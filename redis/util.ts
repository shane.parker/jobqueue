import { HashObject } from './types.ts'
import { Redis } from './redis.ts'

import { RedisPromise } from './types.ts'

type ConverterType = { [k: string]: (v: string) => string | number }
type LoopFn = () => Promise<unknown>

const hashConverter: ConverterType = {
  'string': (v: string) => v,
  'number': (v: string) => parseFloat(v)
}

export const loop = async (fn: LoopFn) => {
  for ( ;; ) {
    await fn()
  }
}

type Deferred<T> = {
  promise: Promise<T>
  resolve?: (v: T) => void
  reject?: (v: T) => void
}

export const deferred = <T>(): Deferred<T> => {
  let resolve: undefined | ((v: T) => void)
  let reject: undefined | ((v: T) => void)

  const promise = new Promise<T>((res, rej) => {
    resolve = res
    reject = rej
  })

  return { promise, resolve, reject }
}

export const getHashObject = <T>(redis: Redis<RedisPromise>, key: string, schema: {[key: string]: string}): Promise<T> => {
  const keys = Object.keys(schema)

  return redis.hget(key, ...keys).then(r =>
    (r as string[]).reduce((acc: HashObject | null, v, idx) => {
      if ( v === null ) {
        return acc
      } else if ( acc === null ) {
        acc = {}
      }

      const key = keys[idx]
      const type = schema[key]
      const value = hashConverter[type]?.(v)
      
      acc[key] = value
      return acc
    }, null)) as Promise<T>
}