import { config } from 'https://deno.land/x/dotenv/mod.ts'
config({ export: true })

import { BufReader } from 'https://deno.land/std@0.170.0/io/mod.ts'

import {
  RedisValue,
  RedisPromise,
  TransactionOptions
} from './types.ts'

import { Redis, makeRedis } from './redis.ts'
import { deferred } from "./util.ts";

const REDIS_HOST = Deno.env.get('REDIS_HOST')!
const REDIS_PORT = Deno.env.get('REDIS_PORT')!

const encoder = new TextEncoder()
const decoder = new TextDecoder()

type QueueFunc = () => Promise<unknown>

type QueueEntry = {
  fn: QueueFunc
  resolve: (v: unknown) => void
  reject: (err: Error) => void
}

export type ConnectionInit = {
  host?: string,
  port?: number
}

export type Pipeline = {
  exec: () => Promise<RedisValue>
}
export type RedisPipeline = Redis<void> & Pipeline

export type Transaction = {
  watch: (...key: string[]) => Promise<void>
  multi: (pl: PipelineFunc) => void
}
export type RedisTransaction = Redis<RedisPromise> & Transaction

export type TransactionFunc = (trx: RedisTransaction) => RedisPromise
export type PipelineFunc = (pl: RedisPipeline) => void

export type TransactionResult = { executed: boolean, result?: RedisValue }

export type Connection = {
  multi: () => RedisPipeline,
  transaction: (fn: TransactionFunc, opts?: TransactionOptions) => Promise<TransactionResult> 
}
export type RedisConnection = Redis<RedisPromise> & Connection

type ParserTable = { [k: string]: (header: string, reader: BufReader) => RedisValue | Promise<RedisValue> }

type PipelineCmd = {
  cmd: string
  args: RedisValue[]
}

export const connection = async ({ host = REDIS_HOST, port = +REDIS_PORT }: ConnectionInit = {}): Promise<RedisConnection> => {
  const conn = await Deno.connect({ hostname: host, port })
  const queue: QueueEntry[] = []
  const reader = new BufReader(conn)

  let execPromise: Promise<void> | undefined

  const execQueue = async () => {
    let entry
    while ( (entry = queue.shift()) !== undefined ) {
      const { fn, resolve, reject } = entry
      try {
        resolve(await fn())
      } catch ( error ) {
        console.log(error)
        reject(error)
      }
    }
    execPromise = undefined
  }
  
  const push = (fn: QueueFunc) => {
    const def = deferred<unknown>()

    queue.push({ fn, resolve: def.resolve!, reject: def.reject! })
    if ( (queue.length === 1) && !execPromise ) {
      execPromise = execQueue()
    }

    return def.promise
  }

  const write = async (cmd: string, ...args: RedisValue[]) => {
    const str = [cmd, ...args.map(v => v!.toString())].map(s => 
      `$${s!.length}\r\n${s}\r\n`)

    const cmdLine = `*${str.length}\r\n${str.join('')}`

    let left = cmdLine.length
    while ( left > 0 ) {
      const chunk = cmdLine.substring(cmdLine.length-left, left)
      left -= await conn.write(encoder.encode(chunk))
    }

    return readResponse(reader)
  }

  const send = <T>(cmd: string, ...args: RedisValue[]) => push(() => write(cmd, ...args)) as Promise<T>
  
  const connection = makeRedis<Connection, Promise<RedisValue>>({
    send: (cmd, ...args) => send<RedisValue>(cmd, ...args),
    multi: () => {
      const cmds: PipelineCmd[] = []

      return makeRedis<Pipeline, void>({
        send: ((cmd, ...args) => {
          cmds.push({ cmd, args })
          return Promise.resolve('QUEUED')
        }),

        exec: () => push(async () => {
          await write('MULTI')
          for ( const { cmd, args } of cmds ) {
            await write(cmd, ...args)
          }
          return write('EXEC')
        }) as Promise<RedisValue>
      })
    },

    transaction: (fn, { retries = Number.POSITIVE_INFINITY } = {}) => {
      const cmds: PipelineCmd[] = []

      const trx = makeRedis<Transaction, Promise<RedisValue>>({
        send: write,
        multi: fn => {
          const pipeline = makeRedis<Pipeline, void>({
            send: (cmd, ...args) => {
              cmds.push({ cmd, args })
              return Promise.resolve()
            },
            exec: () => Promise.resolve([])
          })

          fn(pipeline)
        },
        watch: async (...keys) => {
          if ( cmds.length > 0 ) {
            throw Error('Cannot perform watch inside MULTI')
          }

          await write('WATCH', ...keys)
        }
      })

      return push(async () => {
        let tries = 0
        while ( tries++ < retries ) {
          const fresult = await fn(trx)
          if ( (fresult === false) || (cmds.length === 0) ) {
            return { executed: false }
          }

          await write('MULTI')
          for ( const { cmd, args } of cmds ) {
            await write(cmd, ...args)
          }

          const tresult = await write('EXEC')
          if ( tresult !== undefined ) {
            return { executed: true, result: tresult }
          }
        }

        throw Error('Max commit retries reached')
      }) as Promise<TransactionResult>
    }
  })
  
  return connection
}

const arrayParser = async (header: string, reader: BufReader): Promise<RedisValue[]> => {
  const len = parseInt(header)
  const tmp: RedisValue[] = []

  for ( let x = 0; x < len; x++ ) {
    const v = await readResponse(reader)
    tmp.push(v)
  }

  return tmp
}

const simpleStringParser = (header: string, _reader: BufReader): string => {
  return header.trim()
}

const errorParser = (header: string, _reader: BufReader) => {
  throw Error(header)
}

const numberParser = (header: string, _reader: BufReader): number => {
  return parseFloat(header)
}

const stringParser = async (header: string, reader: BufReader): Promise<string | undefined> => {
  const len = parseInt(header)
  if ( len === -1 ) {
    return undefined
  }

  const buffer = new Uint8Array(len+2)
  await reader.readFull(buffer)

  return decoder.decode(buffer.subarray(0, buffer.length-2))
}

const readResponse = async (reader: BufReader) => {
  const header = decoder.decode((await reader.readLine())?.line)
  const type = header.substring(0, 1)
  
  const parser: ParserTable = {
    '$': stringParser,
    ':': numberParser,
    '+': simpleStringParser,
    '-': errorParser,
    '*': arrayParser
  }

  return parser[type]?.(header.substring(1), reader)
}
