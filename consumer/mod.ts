import { connection } from '../redis/mod.ts'
import { Job, JobSchema } from '../common/mod.ts'
import { getHashObject } from '../redis/util.ts'

export type ConsumerInit = {
  queue: string
  name: string
}

export type WaitDesc = {
  timeout?: number
}

export type JobConsumer = {
  wait: (desc?: WaitDesc) => Promise<Job | undefined>
}

const consumer = async ({ queue, name }: ConsumerInit): Promise<JobConsumer> => {
  const redis = await connection()

  return {
    wait: ({ timeout = 0 } = {}) => {
      const src = `queue:${queue}:${name}`
      const dst = `queue:${queue}:in-progress`

      return redis.blmove(src, dst, 'right', 'left', timeout)
        .then(jid => getHashObject(redis, jid as string, JobSchema))
    }
  }
}

export default consumer
